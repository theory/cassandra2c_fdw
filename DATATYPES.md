Supported Data Types
====================

## Numeric types
tinyint
smallint
int
bigint
float
double

## Boolean types
boolean

## Text types
text
ascii
varchar

## Other types
inet
uuid
