cassandra_fdw
==============

Foreign Data Wrapper (FDW) that facilitates access to Cassandra 2.2+
from PostgreSQL 9.5

Cassandra: http://cassandra.apache.org/

## Install

This FDW is included in the [PostgreSQL by BigSQL](http://bigsql.org) distribution.  All you have to do is follow the usage instructions below.  There are no special pre-requirements, since this CassandraFDW is built with the native cpp-driver.

## Building from Source

In addition to normal PostgreSQL FDW pre-reqs, the primary specific
requirement for this FDW is the Cassandra CPP driver version 2.3
(https://github.com/datastax/cpp-driver).

First, download the source code under the contrib subdirectory of the
PostgreSQL source tree and then build and install the FDW as below:

```
cd cassandra2c_fdw
USE_PGXS=1 make
USE_PGXS=1 make install # with sudo if necessary
```

## Usage

The following parameters can be set on a Cassandra foreign server
object:

  * **`host`**: the address or hostname of the Cassandra server, Examples: "127.0.0.1" "127.0.0.1,127.0.0.2", "server1.domain.com".
  * **`port`**: the port number of the Cassandra server. Defaults to 9042.
  

The following parameters can be set on a Cassandra foreign table object:

  * **`schema_name`**: the name of the Cassandra keyspace to query.  Defaults to "public".
  * **`table_name`**: the name of the Cassandra table to query.  Defaults to the foreign table name used in the relevant CREATE command.

Here is an example:

```sql
    -- load EXTENSION first time after install.
    CREATE EXTENSION cassandra_fdw;

    -- create server object
    CREATE SERVER cass_serv FOREIGN DATA WRAPPER cassandra_fdw
        OPTIONS (host '127.0.0.1,127.0.0.2', port '9042');

    -- Create a user mapping for the server.
    CREATE USER MAPPING FOR public SERVER cass_serv
        OPTIONS (username 'test', password 'test');

    -- Create a foreign table on the server.
    CREATE FOREIGN TABLE test (id int) SERVER cass_serv
        OPTIONS (schema_name 'example', table_name 'oorder');

    -- Query the foreign table.
    SELECT * FROM test LIMIT 5;

    -- Import Cassandra schema as a foreign schema. Requires TEST_SCHEMA
    -- to exist in Cassandra as well as in PostgreSQL.
    IMPORT FOREIGN SCHEMA TEST_SCHEMA
        FROM SERVER cassandra_test_server INTO TEST_SCHEMA;
```
IMPORT FOREIGN SCHEMA

    The Objects imported would follow the same case sensitivity as that
    of Cassandra.  This applies to the other identifiers as well such as
    column names etc. The case sensitivity also applies for the Object
    names referenced in the LIMIT TO and EXCEPT clauses.

Here are some examples:

```sql
    -- The Test_Tab1 was created as case sensitive in Cassandra and
    -- test_tab2 was created as case-insensitive.  Only the tables
    -- "Test_Tab1" and test_tab2 are imported from the Cassandra
    -- TEST_SCHEMA keyspace.  If there are existing objects in the
    -- PostgreSQL FOREIGN SCHEMA TEST_SCHEMA they will not be removed.

    IMPORT FOREIGN SCHEMA TEST_SCHEMA
        LIMIT TO ("Test_Tab1", test_tab2)
        FROM SERVER cassandra_test_server INTO TEST_SCHEMA;

    -- Import all other objects from the Cassandra TEST_SCHEMA schema
    -- except "Test_Tab1" and test_tab2.
    IMPORT FOREIGN SCHEMA TEST_SCHEMA
        EXCEPT ("Test_Tab1", test_tab2)
        FROM SERVER cassandra_test_server INTO TEST_SCHEMA;
```

## Test

Our latest testing uses Cassandra 3.0 against PostgreSQL 9.5
