# cassandra2c_fdw/Makefile

MODULE_big = cassandra_fdw
OBJS = cassandra_fdw.o cass_connection.o

SHLIB_LINK = -lcassandra

EXTENSION = cassandra_fdw
DATA = cassandra_fdw--2.0.sql

REGRESS = cassandra_fdw

ifdef USE_PGXS
PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)
else
subdir = contrib/cassandra2c_fdw
top_builddir = ../..
include $(top_builddir)/src/Makefile.global
include $(top_srcdir)/contrib/contrib-global.mk
endif
